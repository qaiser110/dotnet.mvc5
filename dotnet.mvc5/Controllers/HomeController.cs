﻿using dotnet.mvc5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;

namespace dotnet.mvc5.Controllers
{
    public class HomeController : Controller
    {
        FoodDb _db = new FoodDb();
        
        public ActionResult Index(string searchTerm = null, int page = 1)
        {
            //var model = _db.Restaurants.ToList();

            // Comprehension syntax
            var model1 = 
                from r in _db.Restaurants
                where r.Country == "USA"
                select r;

            // same as above in Extension syntax
            var model_2 = 
                _db.Restaurants
                .Where(r => r.Country == "USA")
                .OrderBy(r => r.Name)
                .Skip(0)
                .Take(10);

            var model_3 =
                from r in _db.Restaurants
                orderby r.Reviews.Count() descending
                select r;

            var model_4 =
                from r in _db.Restaurants
                orderby r.Reviews.Average(review => review.Rating) descending
                select new ResraurantListViewModel 
                {
                    Id = r.Id,
                    Name = r.Name,
                    City = r.City,
                    Country = r.Country,
                    CountOfReviews = r.Reviews.Count()
                };
            // same as above in Extension syntax
            var model_4b =
                _db.Restaurants
                .OrderByDescending(r => r.Reviews.Average(review => review.Rating))
                .Where(r => searchTerm == null || r.Name.StartsWith(searchTerm))
                .Select(r => new ResraurantListViewModel
                            {
                                Id = r.Id,
                                Name = r.Name,
                                City = r.City,
                                Country = r.Country,
                                CountOfReviews = r.Reviews.Count()
                            }
                ).ToPagedList(page, 10);
                    //AverageRating = r.Reviews.Average(review => review.Rating)

            if (Request.IsAjaxRequest())
            {
                return PartialView("_Restaurants", model_4b);
            }

            return View(model_4b);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}