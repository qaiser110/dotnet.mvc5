﻿using dotnet.mvc5.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace dotnet.mvc5.Controllers
{
    public class ReviewsController : Controller
    {
        FoodDb _db = new FoodDb();
        
        // GET: Reviews
        public ActionResult Index([Bind(Prefix="id")] int restaurantId)
        {
            var restaurant = _db.Restaurants.Find(restaurantId);
            if (restaurant != null)
            {
                return View(restaurant);
            }
            return HttpNotFound();
        }

        [ChildActionOnly]
        public ActionResult BestReview()
        {
            var bestReview = from r in _reviews
                                orderby r.Rating descending
                                select r;

            return PartialView("_Review", bestReview.First());
        }

        // GET: Reviews/Create
        public ActionResult Create(int restaurantId)
        {
            return View();
        }
        // POST: Reviews/Create
        [HttpPost]
        public ActionResult Create(RestaurantReview review)
        {
            if (ModelState.IsValid)
            {
                _db.Reviews.Add(review);
                _db.SaveChanges();
                return RedirectToAction("Index", new { id = review.RestaurantId });
            }
            return View(review);
        }

        // GET: Reviews/Edit/5
        public ActionResult Edit(int id)
        {
            var model = _db.Reviews.Find(id);

            return View(model);
        }

        // POST: Reviews/Edit/5
        [HttpPost]
        public ActionResult Edit(RestaurantReview review)
        {
            if (ModelState.IsValid)
            {
                // Tell EntityState to start tracking review entity for any changes
                _db.Entry(review).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index", new { id = review.RestaurantId });
            }
            return View();
        }

        // GET: Reviews/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Reviews/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

        static List<RestaurantReview> _reviews = new List<RestaurantReview>
        {
            new RestaurantReview {
                Id = 1,
                Body = "McDonalds",
                //City = "London",
                //Country = "UK",
                Rating = 3
            },
            new RestaurantReview {
                Id = 2,
                Body = "Neptune",
                //City = "Sydney",
                //Country = "Australia",
                Rating = 8
            },
            new RestaurantReview {
                Id = 3,
                Body = "Sams",
                //City = "N.Y.",
                //Country = "USA",
                Rating = 7
            },
            new RestaurantReview {
                Id = 4,
                Body = "Fridays",
                //City = "Dubai",
                //Country = "UAE",
                Rating = 5
            }
        };
    }
}
