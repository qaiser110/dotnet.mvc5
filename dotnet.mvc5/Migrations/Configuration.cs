namespace dotnet.mvc5.Migrations
{
    using dotnet.mvc5.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<dotnet.mvc5.Models.FoodDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "dotnet.mvc5.Models.FoodDb";
        }

        protected override void Seed(dotnet.mvc5.Models.FoodDb context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            for (int i = 0; i < 1000; i++)
            {
                context.Restaurants.AddOrUpdate(r => r.Name,
                    new Restaurant { Name = i.ToString(), City = "Nowhere", Country = "Noland" });
            }

            context.Restaurants.AddOrUpdate(r => r.Name,
                new Restaurant { Name = "Sabrino's", City = "Baltimore", Country = "USA" },
                new Restaurant
                {
                    Name = "New One",
                    City = "NJ Pop",
                    Country = "USA",
                    Reviews = new List<RestaurantReview>
                    {
                        new RestaurantReview {Rating = 9, Body = "Great Food!"},
                        new RestaurantReview {Rating = 7, Body = "Good Food!"},
                        new RestaurantReview {Rating = 9, Body = "Great!!!"},
                        new RestaurantReview {Rating = 8, Body = "Great!"},
                    }
                },
                new Restaurant { 
                    Name = "New Two", 
                    City = "NJ", 
                    Country = "USA",
                    Reviews = new List<RestaurantReview>
                    {
                        new RestaurantReview {Rating = 9, Body = "Great Food!"},
                        new RestaurantReview {Rating = 7, Body = "Good Food!"},
                        new RestaurantReview {Rating = 6, Body = "Great!!!"},
                        new RestaurantReview {Rating = 5, Body = "Great!"},
                    }
                }
            );
        }
    }
}
