﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(dotnet.mvc5.Startup))]
namespace dotnet.mvc5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
